---
title: "{{ replace .Name "-" " " }}"
date: {{ .Date }}
draft: true
categories: ["faire des trucs"]
tags: ["pain", "Perséphone", "levain"]
slug: {{ .Name }}
postimage: ""
postimagedescription: ""
---

Pain de la semaine, ou les aventures de Perséphone.

Farine(s) : \
Hydratation : Environ xx g d'eau pour 100 g de farine. \
Fermentation : \
Poids final : xxx g.

{{< figure src="images/.jpg"
           alt=""
           caption="" >}}
