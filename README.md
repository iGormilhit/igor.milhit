# [igor.milhit.git][title]

⚠ **Ce dépôt à été archivé. Il est désormais disponible sur
<https://git.milhit.ch/igor/igor.milhit/>**

Depuis le début de [mon site personnel][sitePerso], qui n'était au début qu'une
simple page HTML faite à la main, j'ai travaillé dans un dépôt `git`. Les
sources ont toujours été forcément publiques, puisqu'il ne s'agit que de HTML
et de CSS. Puis, je me suis dit que je pouvais travailler de manière plus
ouverte. Depuis la version `v2.0.0`, j'utilise le générateur de site statique
HUGO, afin d'y adjoindre un blog et des pages statiques.

## Licence

Ce projet est sous licence `CC-BY` :
<http://creativecommons.org/licenses/by/4.0/>

## Thème

Je construis peu à peu mon propre thème pour HUGO. Bien qu'en cours
d'élaboration, le thème est disponible en ligne : [portfoliGor][portfoliGor],
qui devrait bientôt migrer sur <https://git.milhit.ch>.

[title]: https://git.milhit.ch/igor/igor.milhit
[sitePerso]: https://igor.milhit.ch
[portfoliGor]: https://framagit.org/iGormilhit/portfoliGor
