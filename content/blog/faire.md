---
title: "Faire"
date: 2021-11-29T18:34:42+01:00
publishdate: 2021-12-02
draft: false
categories: ["explorations", "faire des trucs"]
tags: ["Lordon", "capitalisme", "désir", "faire"]
slug: faire
---

{{< blockquote author="Frédéric Lordon" lang="fr"
               title="En travail : conversations sur le communisme, p. 60"
               link="https://ladispute.fr/catalogue/en-travail-conversation-sur-le-communisme/" >}}
En réalité c'est le capitalisme qui produit le désir « de ne rien faire », mais
de ne rien faire *pour lui*, comme désir de se soustraire à son faire ignoble, à
sa coercition de faire (pour le jeu de mots). <mark>Le coup de force
invraisemblable, mais c'est celui-là même de la convention capitaliste de la
valeur, c'est d'avoir rabattu tout « faire » sur « faire pour le capital ». En
en « déduisant » que ne pas faire pour le capital, c'était ne pas faire du
tout. Mais les humains ne sont pas comme ça : puisque vivre c'est désirer,
désirer faire -- et faire. Alors ils font -- des tas de choses, et chacun selon
son désir.</mark> C’est là que toi [Friot] tu arrives avec ta convention
communiste de la valeur qui dit : ce que les humains font dans la société,
c'est toujours de quelque manière utile à la société, c'est bon pour elle (sous
réserve de quelques critères de légalité bien sûr…). *Ergo* ça a de la valeur
en soi.
{{< /blockquote >}}

La question n'est donc pas de savoir comment nous mettre en mouvement *contre
notre gré*, si un salaire à vie, sans condition de poste de travail, nous était
assuré, en tant que reconnaissance de notre qualité de producteur et
productrice. Le fait est que la contrainte de l'emploi nous *gâche* le travail,
même s'il ne peut pas être seulement une partie de plaisir. Même dans le mode
de production capitaliste où le travail ne nous appartient pas, nous sommes
capables d'éprouver des émotions positives, gratifiantes, lorsque nous sommes
en mesure de faire bien quelque chose. Alors imagine si nous avions
collectivement la souveraineté sur le fait de faire quelque chose, si tout
« faire » pouvait devenir fertile, reconnu. Et si notre salaire ne dépendait
pas de tel ou tel « faire »[^1].

*Pouvait*, parce qu'on n'échappe pas au fait de devoir décider collectivement
des « faire » qu'on ne voudrait plus laisser faire, comme par exemple détruire
les possibilités d'existence de la pie grièche pour augmenter la rentabilité à
l'heure d'une exploitation agricole[^2], et qu'en plus, collectivement, on ne
sera jamais d'accord sur tout. Pour autant, je trouve préférable de vivre ces
échanges, les frustrations qui vont avec, d'explorer les manières de décider
ensemble en évitant la bête tyrannie de la majorité, en évitant de renforcer
les inégalités sociales existantes (par exemple le patriarcat), plutôt que de
laisser une minorité, parce qu'elle détient le capital économique et social de
nous imposer ses délires morbides.

Ce qui m'intéresse le plus dans la proposition du salaire à vie, c'est cela :
notre souveraineté collective et *la plus démocratique possible* sur la
production. Ne plus accepter d'être exclues et exclus d'une part importante de
la sphère de la politique. Agir et être responsables. Refuser d'être à ce point
diminuées et diminués dans notre devenir humain et ce au profit d'une classe,
même si elle a participé à nous extraire de la monarchie absolue.

La référence exacte :

{{< smallcaps "Friot" >}}, Bernard et {{< smallcaps "Lordon" >}}, Frédéric,
2021. *En travail : conversations sur le communisme*. Paris : La Dispute.
Entretiens. ISBN 978-2-84303-322-3.

[pie]:https://www.youtube.com/watch?v=qdXO4eKGMBU "Un oiseau serial killer dans nos campagnes ? 😱"

[^1]: Ce qui n'enlève pas grand chose que la production de ce qui nous permet
de vivre doit être fait, sinon il ne va pas y avoir grand chose à cotiser et à
mettre en commun.
[^2]: Voir [cette vidéo sur Youtube][pie].
