---
title: "Lundi, existence"
date: 2019-09-23T06:19:04+02:00
publishdate: 2019-09-23
draft: false
categories: ["traces"]
tags: ["lundi", "angoisse", "existentiel"]
slug: lundi-existence
---

Dans la nuit du matin d'automne, même précoce, sous la roue du vélo
à assistance animale, sur le bitume phonoabsorbant, aussi noir que les berlines
de la transition énergétique par habitude de l'ivresse, l'odeur de l'angoisse
existentielle imprègne l'ensemble des fibres de mes textiles innovants.
Existence insoutenable, jusque dans l'intime de chaque détail.

Pourtant, là n'est pas la question de mon embourbement individuel, ce moi qui
ne parvient pas à se donner l'illusion de l'action, de la décision. Trivial. Un
Tue-La-Poésie dans lequel je pourrais bien dénicher des germes de poésie. Le
plaisir de laisser des traces éphémères, un acharnement dérisoire, le salut
naît de la dérision, faire face à la manière d'un funambule, le vide transpire
de partout, de toutes les directions, au travers des pores de tout ce que l'on
érige entre soi et l'existence, ce prédateur, ce chat joueur qui ne connaît pas
la pitié.

Car tout meurt, tu meures, tu es mort, la fête est finie, il faut se réjouir
justement, remuer la terre, les entrailles, combiner les saveurs, tailler dans
le mou, dans le fuyant, courir sur les sables mouvants, gonfler sa poitrine de
cette vie impossible, de cette conscience encombrante, qui voit sans voir, qui
sait sans savoir ou ignore en sachant, tout ces mots pour sauver la face,
provoquer l'étincelle, s'incendier l'amazonette, retourner à la mine avec vue
sur des montagnes de silicium, aligner des petits cailloux et se réchauffer le
cœur.
