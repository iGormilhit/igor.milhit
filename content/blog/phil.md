---
title: "Phil"
date: 2019-06-03T06:36:42+02:00
draft: false
categories: ["traces"]
tags: ["vie", "mort", "Phil", "deuil"]
---

Quand tu connais l'histoire, tout prend un sens différent. Ou plutôt, c'est enfin le moment de donner du sens à ce qui s'est vécu, non pas de manière insensée, mais un instant avant le foisonnement du sens, un instant si éphémère qu'il pourrait bien ne pas avoir existé. Phil n'est plus. Phil est mort. Un des axes de ma planète a disparu, pourtant elle tourne. Je n'avais pas tout à fait conscience à quel point, Phil, tu étais l'un de ces axes. Peut-être n'ai-je jamais fait attention à la fragilité de mon ami, peut-être pas suffisamment. Peut-être aussi qu'il s'agit de ma fragilité. Plutôt. Sûrement.
Phil, c'était celui sur qui on pouvait compter, toujours. Être un recours, ce n'était pas négociable. Au sommet des priorités. Et c'est difficile, aujourd'hui, de savoir si j'ai été à la hauteur. Et aussi, à sa manière certes, il savait accueillir, t'accepter tel quel. C'est une qualité précieuse, qu'il poussait à un degré assez rare sur cette terre.

Peut-être bien parce qu'il donnait de l'importance à ce qui relie les humains entre eux. Il vivait pas mal seul, c'est vrai, souvent déçu, blessé, par notre imperfection, notre complexité, notre ambigüité, nos compromissions. Mais contrairement à ce qu'il pouvait dire, il n'était pas du tout asocial. Il savait aussi entretenir les liens. Avec les gens. Avec une partie du réel, et plutôt avec un réel des profondeurs, par opposition à la surface, au superficiel.

Je ne sais pas si ça fait du sens pour vous, mais c'est bien le souvenir qu'il me laisse. Ce qui n'épuise pas toutes les facettes du bonhomme. Il était *très* consciencieux, en réalité. Trop peut-être, pour le marché du travail. Il en a fait des jobs, et il les a bien fait. En échange du minimum social, pas même un salaire minimal. Souvent, on lui a promis un poste, un poste qui n'est jamais venu. Y a des trucs qu'on ne reçoit jamais sans les prendre d'abord. C'était peut-être difficile pour lui de penser qu'il y avait aussi droit. Que ce n'était pas incompatible avec le refus des injustices et des incohérences de notre façon de vivre ensemble.

Et puis, s'il avait une profession, c'était avocat du diable. En discutant avec lui, on s'est retrouvé parfois à faire de sacrées pirouettes, on ne savait plus très bien au final qui défendait quelle position au juste.

En fait, j'aimerais dire des tas de choses, autre chose, mais j'ai le sentiment de faire un détournement d'attention et de fixer une image de Phil qui ne correspond pas au Phil en train de vivre, en train de chercher à souffler, de chercher à comprendre, de chercher à évoluer. La vie qu'il menait le mettait face à lui-même, de manière urgente. Il aurait pu regarder ailleurs, fuir encore, toujours plus loin. Je sais que ce n'est pas ce qu'il faisait, non. Et je soupçonne que c'était d'autant plus difficile que ça lui demandait de contredire des choses fondamentales sur lesquelles il avait pu se construire et qui désormais l'étouffaient. Hé bien, il avait accepté d'envisager ce chemin. Personne ne peut dire que c'est facile. Personne.

Oui, c'est aussi ce qui fait mal aujourd'hui. Dans tous les cas, ça ferait mal, avec cette histoire ou une autre. Mais je ne peux pas m'empêcher de penser qu'il faisait preuve de courage. Franchement, ici, c'est bien la mort qui s'est montrée lâche. En tout cas, c'est ce que je pense.

Mais on ne va pas finir là-dessus, ce serait trop bête et pas fidèle envers Phil. Je préfère me souvenir de son rire, de son sourire, par exemple lors d'un repas dans un restaurant coréen aux Pâquis, devant un grand écran plat qui diffusait des clips de K-pop, en train de parler d'un manga ou d'un roman de science fiction. C'était chouette de partager des moments avec Phil.
