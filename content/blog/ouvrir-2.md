---
title: "Ouvrir à nouveau"
date: 2021-09-12T10:24:59+02:00
draft: false
categories: ["traces"]
tags: ["ouvrir", "écrire", "marcher", "explorer"]
slug: ouvrir-a-nouveau
postimagedescription: "Vue d'un lac et de montagnes au loin."
postimage: "ouvrir-2.jpg"
description: "S'ouvrir à nouveau à soi-même."
---

Faire des listes. Lister les infinitifs, les infinis définitifs. Retrouver le
clavier, le blog. Reprendre les mots dans ses bras. Se lover dans les bras des
mots. En faire une base solide pour explorer les terres toujours renouvelées
des émotions, les terres en continue création des pensées. Surtout, déjouer les
impératifs qui avancent camouflés. La lutte continue.

Et le maître zen du stéréotype qui se marre. « Sans lutter, bien sûr ! »

Peut-être définir un peu plus, afin d'ouvrir portes et fenêtres aux imprévus, à
l'improvisation, aux embûches. Se remettre à marcher dans l'espoir de
trébucher, éraflures aux genoux, premières lignes tracées il y a des siècles et
des siècles.

Il y a 15 ou 20 milliers de siècles.

De mémoire de primate humain.

Raconter les nuances et les grands écarts, les dérives génétiques et
historiques entre ce que ce je croit pouvoir comprendre des mondes qu'il
habite, qui le peuplent, et ce que ce je croit pouvoir comprendre de lui-même.
Dire ses propres radicalisations, ses propres clivages, afin de touiller ce qui
apparaît, remonte à la surface et tenter des émulsions, faire monter la
mayonnaise comme la sauce à salade.

Ou simplement, se faire un petit *caffè*.
