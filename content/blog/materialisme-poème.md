---
title: "De l'origine de la poésie"
date: 2020-11-20T15:32:11+01:00
publishdate: 2021-01-08
draft: false
categories: ["poésie", "traces"]
tags: ["matérialisme", "ontologie", "émergence"]
---

ré enchanter le monde dés enchanter le monde coup du sort le truc c'est qu'il
n'y en a pas pas de sortilège pas de dés ordres dans les arts partis à la plage
à péage les arpèges s'emboîtent en toute nécessité création contrainte par
l'obstacle incontournable et pourtant aussi inaccessible que l'étoile toujours
au bout du bout de la langue autour de la membrane entre réel et fictions entre
la pulpe des doigts mieux que le sable dans le rouage l'huile de coude dés à
coudre à suivre la trame filer la métascience de travers le sujet c'est dire
énoncer le monde des enchantés des pouilleux d'épouilleuses l'absence épanouit
ce qui reste d'un rien se tire par les cheveux les terreaux les tourbes les
troubles invasion du souffle du cœur enlève lui l'âme et tout s'anime naît
encore et à nouveau lever de soleil un solstice d'hiver road trip céleste ton
char en panne dans le grand nord je suis sûr qu'elle aurait rigolé elle rigole
dans ma tête la voilà l'éternité il faut la perdre et ne jamais la retrouver
comme la morale des il faut à fondre dans les moules mous des heures et des
saisons la raison des reflux et flux la mer l'estuaire l'océan l'eau céans que
le sens relie système de systèmes d'où émerge ce qui n'y est pas l'impossible
sorti des eaux un possible à la chevelure de méandres mal attendu la puce la
surprise à l'oreille les écrits racontent des histoires qui n'ont pas été
vécues mais puisque que le sujet le sujet te dis-je le sujet c'est dire hé non
c'est énoncer renoncer à la mise en boîte précisément ce qui te mettra en bière
échappe prend la tangente à un poil des approximations pourtant c'est
l'approche qui compte nous met d'accord dissonance tension vers l'asymptote la
sainte ôte l'épine du pied et continue sa pérégrination ré enchanter le monde
dés enchanter le monde coup du sort le truc c'est qu'il n'y en a pas pas de
sortilège pas de dés ordres
