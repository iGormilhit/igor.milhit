---
title: "Une boucle"
date: 2020-03-26T14:26:17+01:00
publishdate: 2020-04-01
draft: false
categories: ["poésie"]
tags: ["#ecritHebdo", "écriture"]
slug: une-boucle
layout: verse
---

une \
position puis un déplacement \
pour produire du sens \
sur un autre plan \
ici ne se montre \
que le procédé nu \
un espace contraint en \
boucle

---

<small>

Thème de la semaine : <https://imaginair.es/@Ezelty/103888233842545452>

À fouiller :

- <https://mstdn.fr/tags/EcritHebdo>
- <https://imaginair.es/tags/EcritHebdo>
- <https://pouet.it/tags/EcritHebdo>
</small>
