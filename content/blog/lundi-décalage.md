---
title: "Lundi, décalage"
date: 2019-10-28T07:13:34+01:00
draft: false
categories: ["traces"]
tags: ["lundi", "décalage"]
slug: lundi-décalage
---

Comme un lundi, dans un train expulsé par l'aube assassinée, un train
surchauffé pour mieux étouffer l'automne, comme un lundi étrange, surpeuplé, un
lundi de prolétaires qui ne veulent rien savoir, nous sommes la classe moyenne,
on n'y croit dur comme fer, comme épargne obligatoire à la chaleur des taux
négatifs, comme neige remplacée par les gaz à effet de serre, comme un lundi
déjà obsolète, fossile, à la hauteur de rien.

Inadéquat.

Hier, nous nous racontions la vie des anciens, des anciens fort jeunes, ils et
elles viennent à peine de mourir, les traces de leur vie sont encore fraîches,
leur vie a laissé des traces justement, ne serait-ce que des souvenirs, des
histoires, des bribes de récit. Alors que les nôtres ? Je ne sais pas pour toi,
mais de mon côté rien de bien digne d'être raconté. Aucune différence, à peine
un effort absurde de comprendre le monde. Et rien.

Parce qu'il faudrait simplement s'arrêter, refuser de continuer ainsi, puisque
l'on sait que cette voie est une impasse, l'impasse de la fosse commune.
