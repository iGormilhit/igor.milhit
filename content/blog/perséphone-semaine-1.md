---
title: "Pain de la semaine (1)"
date: 2021-12-15T16:57:20+01:00
draft: false
categories: ["faire des trucs"]
tags: ["pain", "Perséphone", "levain"]
slug: pain-semaine-1
postimage: "persephone-2021-12-15.jpg"
postimagedescription: "Photo coupée d'une boule de pain dans son banneton."
---

Pain de la semaine, ou les aventures de Perséphone.

Farine(s) : Rouge de Bordeaux [des Maîtres de Mon Moulin][1]. \
Hydratation : Environ 70 g d'eau pour 100 g de farine. \
Fermentation : Courte (3 heures et demie). \
Poids final : 744 g.

{{< figure src="/images/persephone-2021-12-15-entier.jpg"
           alt="Boule de pain dans son banneton."
           caption="Boule de pain dans son banneton" >}}

{{< figure src="/images/persephone-2021-12-15-rouge-bordeaux.jpg"
           alt="Zoom rapproché de la farine utilisée."
           caption="Rouge de Bordeaux" >}}

[1]: https://lesmaitresdemonmoulin.com/ "Site des farines de meules des Maîtres de Mon Moulin"
