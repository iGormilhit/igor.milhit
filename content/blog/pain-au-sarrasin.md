---
title: "Pain au sarrasin"
date: 2021-02-12T21:18:58+01:00
publishdate: 2021-02-14
draft: false
categories: ["faire des trucs"]
tags: ["pain", "sarrasin", "sans gluten", "levain", "fermentation"]
slug: pain-au-sarrasin
---

Je fais ce pain depuis le printemps 2020. Le pain sans gluten, c'est une longue
histoire. En effet, depuis pas mal d'années maintenant, mon amie est
intolérante au gluten. Elle s'en passerait bien, mais voilà, on fait avec. On a
trouvé ici ou là des substituts, avec plus ou moins de bonheur. Et des recettes
mais toujours avec plein de farines différentes, voir des œufs ou des
ingrédients exotiques, et pas souvent avec des résultats satisfaisants.

Et puis, je me suis intéressé à faire du pain au levain. Du pain au blé, à
l'épeautre, au petit épeautre. Au gluten, donc. Oui, je sais, selon les
farines, il y en a moins, avec la fermentation au levain, ça devrait être plus
digeste, mais ça reste un pain au gluten.

Or, en m'intéressant au pain au levain, je suis tombé sur Thomas
Teffri-Chambelland, notamment sa vidéo sur [le pétrissage manuel][1] et ses
livres :

TEFFRI-CHAMBELLAND, Thomas et DOBOIN, Nathaniel, 2018. *Une autre idée du
pain : des rizières aux délicieuses recettes naturellement sans gluten*.
Paris : La Martinière. ISBN 978-2-7324-7829-6.

TEFFRI-CHAMBELLAND, Thomas, 2019. *Traité de boulangerie au levain*. Paris :
Ducasse éditions. ISBN 978-2-84123-992-4.

En suivant ses recettes (voir [en fin de billet][2]) et en adaptant, j'en suis
arrivé à la méthode présentée ici.

[1]: https://www.youtube.com/watch?v=FkvjdqkZWqg
[2]: #les-recettes

## Levain

J'ai démarré à partir de mon levain chef au seigle. J'en ai prélevé une partie,
et j'ai commencé à le rafraîchir avec de la farine de riz, puisque j'ai
commencé par des essais de pains au riz. Mais avec le sarrasin, le principe est
identique. Je m'en tiens à un levain à 100 %, à savoir le même poids en farine
qu'en eau. Et, contrairement aux recettes de Teffri-Chambelland, je ne mets pas
de farine de psyllium dans le levain. Dans le pain, oui.

Comme il le dit, en quelques rafraîchis, il n'y a plus de seigle dans le
levain. Je ne m'amuserais pas avec quelqu'un qui est allergique au gluten par
contre. Il est certainement possible de démarrer un levain directement avec du
riz ou du sarrasin. Des témoignages existent sur le web, il faudrait que
j'essaie une fois.

Pour être clair, si j'ai 50 grammes de levain, je lui ajoute 25 grammes de
farine et 25 grammes d'eau. Mais quand je fais mes rafraîchis pour préparer un
pain, je m'arrange pour obtenir ce dont j'ai besoin au moment de faire le
mélange, plus une marge pour en mettre de côté, quitte à ce que le dernier
rafraîchi fasse plus que doubler les quantités.

Comme je fais du pain une fois par semaine, quand j'ai fait mon pain, je mets
de côté un peu de levain dans un pot fermé au frigo. Et le matin de la veille
du jour où je fais mon pain, je fais un rafraîchi et je le laisse à température
ambiante. De même le soir, et une troisième fois le matin avant de faire le
pain. Et deux heures après le dernier rafraîchi, je prépare la pâte.

## Ingrédients

Je fais des pains d'un peu moins de 500 grammes. Ce qui donne en termes
d'ingrédients et de quantités :

- 175 grammes de farine de sarrasin, farine complète. Si elle est locale et
  bio, c'est plus sympa. 🤓
- 175 grammes de levain.
- 175 grammes d'eau, chauffée à 30 ou 40 degrés Celsius, selon la température
  ambiante.
- 3 grammes de farine de psyllium (ou de farine de téguments de psyllium, c'est
  encore mieux).
- 3 grammes de sel. J'utilise de la fleur de sel. Parce que.
- De l'huile pour huiler le moule à pain. Un moule à cake peut convenir pour
  commencer, mais une fois qu'on est convaincu, on a plutôt envie d'avoir un
  moule adapté à ses propres besoins.

D'après ce que je comprends, la quantité de levain et l'eau chauffée, ça permet
de raccourcir le temps de la fermentation, ce qui est nécessaire puisque le
sarrasin est une farine fragile.

## Préparation

Par manque de place et parce que j'aime bien aussi sans, je fais le pain sans
robot pour pétrir, aussi j'utilise un grand saladier et une spatule en bois. Je
commence par peser l'eau (que j'ai mis de côté quelques heures plus tôt,
souvent la veille au soir, afin  que le chlore s'évapore), et je la chauffe à
feu très doux, puisqu'il s'agit d'atteindre 30 degrés, pas de porter à
ébullition.

Je prépare mon moule, en l'huilant.

J'ajoute la farine, le sel, la farine de psyllium et le levain dans le
saladier. Puis l'eau. Et je travaille à la spatule. Environ 5 minutes. Je
vérifie surtout que la pâte a la bonne consistance, ni solide, ni liquide.
L'idée, c'est qu'elle soit suffisamment visqueuse pour capturer les
gaz de la fermentation. C'est l'utilité de la farine de psyllium.
Teffri-Chambelland ajoute aussi un peu d'huile d'olive. D'expérience, je trouve
que ce n'est pas utile.

Je verse la pâte dans le moule et je couvre avec un linge. Je laisse reposer à
température ambiante, l'hiver dans un coin un peu tempéré, pendant une heure et
demie, deux heures. Il doit avoir bien gonflé.

## Cuisson

Je préchauffe le four à 250 degrés Celsius. Je fais quelques grignes pas trop
marquées avec une lame de rasoir et j'enfourne pour 28 minutes. C'est un peu
plus fort et plus court que les recettes, mais c'est plus intéressant au goût,
à mon avis. Et j'ajoute encore 8 minutes, à 200 degrés Celsius, la porte du
four entrouverte.

Voilà, c'est cuit. Ne reste plus qu'à démouler et à attendre que le pain soit
refroidi.

## Les recettes

Ci-dessous se trouvent les recettes que j'ai utilisées pour démarrer, à la fois
quelques points sur le levain, un pain au sarrasin avec de la levure et un pain
de sarrasin pur levain (celui que je fais).

Tout d'abord, les scans du livre, au format image (PNG), puis le contenu
textuel des images, au format PDF.

- [Pain au sarrasin à la levure (PNG)][3].
- [Levain jeune de riz (PNG)][4].
- [Pain au sarrasin pur levain (PNG)][5].
- [Contenu textuel des trois images ci-dessus][6] ([sources en Markdown][7])

[3]: /images/autre-idee-pain-p-94-pain-sarrasin.png
[4]: /images/traite-de-boulangerie-levain-sarrasin-p-132.png
[5]: /images/traite-de-boulangerie-pain-sarrasin-p-144.png
[6]: /documents/recettes-pain-sarrasin.pdf
[7]: /documents/recettes-pain-sarrasin.md
