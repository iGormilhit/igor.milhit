---
title: "Saison intérieure"
date: 2020-10-25T20:16:03+01:00
draft: false
categories: ["traces"]
tags: ["saison", "mort", "cendres", "humus"]
---

Déjà [six semaines][1]. Pour autant que les mots qui *ne* me viennent *pas*
soient publiés dans un futur relativement proche. J'écris moins souvent que je
ne [cours][2], sans surprise. Encore que j'accumule les brouillons dans
l'antichambre de ce blog, dans les feuillets d'un autre carnet. Des traces qui
laisseront encore moins de [traces][3], le temps d'une pluie, d'un tapis
flamboyant d'octobre et retour à l'humus et enfin devenir terreau.

*Mais que fait-on de toutes ces cendres ?*

Bien que là, dehors, se consument les dernières flammes de l'automne, ici,
dedans, c'est déjà l'hiver, presque plus rien ne bouge, un hiver forcé, rien à
voir avec le repos, plutôt un repli, le réflexe de l'escargot face à la
curiosité du doigt de l'enfant, face à l'autorité du doigt de la mort, face au
chaos qui déborde de toute part, chahuté, encerclé, j'aurais bien voulu me
rendre, mais personne…

[1]: /quelques-notes
[2]: https://runalyze.com/athlete/ignami
[3]: /categories/traces
