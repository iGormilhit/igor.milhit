---
title: "Pain de la semaine (2)"
date: 2021-12-26T15:29:43+01:00
draft: false
categories: ["faire des trucs"]
tags: ["pain", "Perséphone", "levain"]
slug: pain-semaine-2
postimage: "persephone-2021-12-26.jpg"
postimagedescription: "Gros plan sur une grigne dorée et ouverte, avec en arrière plan un deuxième pain dans son banneton."
---

Pain de la semaine, ou les aventures de Perséphone.

Farine(s) : ¾ de pétanielle noire de nice, ¼ de grands épeautres anciens.  \
Hydratation : 67 g d'eau pour 100 g de farine. \
Fermentation : courte (3 heures et demie pour l'apprêt, 1 heure et demie pour le
               pointage). \
Poids final : 676 et 683 g.
