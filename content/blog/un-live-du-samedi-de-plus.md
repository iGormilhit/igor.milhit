---
title: "Un live du samedi de plus"
date: 2020-04-24T14:33:33+02:00
draft: false
categories: ["musique"]
tags: ["streaming", "listening", "live"]
slug: un-live-du-samedi-de-plus
---

*Mise à jour*

Le *live* (d'*i.m.x.*) a été publié, avec la liste des morceaux. Voici le lien
pour y accéder directement : [5th live](https://id-libre.org/live#fifth).

Le lecteur ci-dessous fonctionne également :

{{< published src="https://id-libre.org/audio/5th-live" legend="5th Live" >}}

---

J'annonce ici un nouveau live pour le **samedi 25 avril 2020, 17:30 (UTC+2)**.
On ne sait pas encore qui sera aux platines, *Radio Relai* ou *i.m.x.*, mais
peu importe, il y aura de la musique sur les autoroutes de l'information !

Oui, ça vous périme rapidement, ce genre d'expression.

Le live sera disponible sur sa page habituelle, à savoir
[https://id-libre.org/live](https://id-libre.org/live), mais ça
devrait tout aussi bien fonctionner avec le lecteur ci-dessous :

{{< stream legend="Le live du samedi 25 avril, dès 17:30" >}}

Je mettrai à jour ce billet lorsque le live version enregistrée et documentée
sera publié.
