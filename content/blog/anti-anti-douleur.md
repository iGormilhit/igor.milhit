---
title: "Anti-anti-douleur"
date: 2019-10-24T16:49:30+02:00
publishdate: 2019-10-25
draft: false
categories: ["traces"]
tags: ["douleur", "deuil", "épaisseur", "présence"]
slug: anti-anti-douleur
---

Les pieds déroulent le récit en épines de mélèzes, géants séculaires qui te
serrent la gorge et saluent dans le feu l'été incinéré au pied de l'automne,
s'apprêtent à migrer, plus haut, ailleurs, ou nulle part, quand on dit que tout
change, c'est aussi que tout fini, c'est aussi que tout change, ça commence
aussi, parfois, tant que ça dure, mais c'en est fini, c'est ainsi. Et qu'est-ce
que la vie, celle qui reste, abandonnée, si ce n'est la rébellion, un point
levé à la surface de l'océan tumultueux, derrière le mur, barbelés et circuits
imprimés, qu'est-ce que la vie si ce n'est d'être submergé par le quotidien pour
lequel il n'existe pas d'insulte à la hauteur de l'arrache-cœur et cette
planète qui ressemble chaque jour de plus en plus à une terre lointaine, d'un
autre système solaire, à l'autre bout de la galaxie, elle-même perdue dans un
tout qui n'est rien tellement il est inconcevable, hein ?

Pourtant, tout va bien. Oui, ici, tout va bien, on ne peut pas se plaindre, on
se plaint que tout aille si bien, on sait que ça ne va pas durer éternellement,
et ce n'est pas même le souci. On se sent un peu coupable d'ailleurs, parce que
si ça va si bien ici, c'est parce que ça va si mal ailleurs. Et ce n'est pas
même le souci. Le souci, c'est que les constructions abstraites, virtuelles et
psychiques ne cessent de s'écrouler et qu'il est nécessaire de sans cesse les
construire à nouveau, mais il faut les réinveter à chaque fois, sur la base des
essais et des erreurs, pas d'essais sans erreurs, console-toi et enroule-toi
dans leurs souvenirs comme dans une vieille couverture de feutre, c'est ce
qu'on appelle l'expérience. \
Pourtant, tout va bien. Non ?

Sauf que j'ai mal et refuser cette douleur n'aide pas, bien au contraire. Il va
falloir vivre dans l'inconfort, le temps des renaissances et des victoires est
terminé, non pas que tout plaisir ne soit plus possible, mais un autre seuil se
présente et ce n'est déjà plus tout à fait vers un monde plus vaste. Voici venu
le temps et l'espace de la limite, du choix, du repli. Et avant d'y creuser une
philosophie qui sache affronter la bourrasque, debout et nu, j'ai encore besoin
d'explorer la courbure de l'espace-temps déformé par le deuil.

