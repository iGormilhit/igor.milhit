---
title: "portfoliGor, première version"
date: 2020-06-01T16:08:12+02:00
draft: false
categories: ["publication numérique"]
tags: ["portfoliGor", "thème", "hugo"]
slug: portfoliGor-première-version
---

En réalité, bien que ce soit encore pas grand chose, bien que ce ne soit pas
encore véritablement un thème utilisable pour [Hugo](https://gohugo.io "Le site
officiel du générateur de site statique Hugo"), je suis très content de cette
[première publication de version](https://framagit.org/iGormilhit/portfoligor/-/releases/0.1.0 "Annonce officielle sur la forge framagit")
du thème de ce site, à savoir *portfoliGor*,
même si elle a été déjà suivie
[d'un correctif](https://framagit.org/iGormilhit/portfoligor/-/releases/0.1.1).

Que fait ce thème ? À peu près le minimum. Bien sûr, il ne s'adapte pas trop
mal à quelques tailles d'écran, en gros ton téléphone, la tablette des gens qui
utilisent cette chose étrange et les écrans des ordinateurs portables ou de
bureau. Pour l'instant, c'est assez facile, vu la complexité minimal de
l'affichage en général.

Dans un premier temps, j'ai simplement voulu reproduire ma page personnelle,
en lui ajoutant une section « blog ». Ainsi, la page d'accueil consiste en une
présentation personnelle basique, avec la liste des moyens de communication,
que ce soit dans la vie privée ou la vie professionnelle, une liste de comptes
en ligne. Les listes peuvent être renseignées dans les paramètres du site,
tandis que des parties textuelles peuvent être rajoutées au moyen d'une page de
contenu (genre `/content/_index.md`).

Je me suis compliqué un poil la tâche afin de pouvoir faire ce que je voulais
avec [les dates](/les-dates-avec-hugo), à savoir afficher d'abord la date de
publication, qui dans certains cas peut être la date ajoutée automatiquement
par la commande `hugo new`, mais faut avouer que c'est rarement, et la date de
la dernière modification si une telle modification a eu lieu.

Comme je suis bibliothécaire, il a fallu que j'ajoute des métadonnées, pour
l'instant c'est encore assez minimal, quelques éléments de
[schema.org](https://schema.org) liés à la personne, moi en l'occurrence, ainsi
qu'une description sommaire de chaque billet de blog en [dublin
core](https://www.dublincore.org) et en [Open Graph Protocol](https://ogp.me/),
d'abord pour être reconnu par [Zotero](https://www.zotero.org "Zotero, what
else?"), avec [un peu d'aide](https://pouet.it/@im/104212964441495738 "une
discussion sur le sujet sur le fediverse").

Et sinon, pas grand chose, pas de police embarquée, pas de trucs fun et
modernes, pas de couleurs, pas même de `README.md` correct, même si j'ai
rapidement ajouté un [journal des
modifications](https://framagit.org/iGormilhit/portfoligor/-/blob/dev/CHANGELOG.md
"le CHANGELOG quoi") avec le correctif. Il faut bien admettre qu'un thème si nu,
et bien, ça pèse pas lourd et ça se charge vite dans ton navigateur.

Pour la suite, il faudra ajouter un peu de documentation, améliorer la
lisibilité, mieux penser la navigation, et tenter de rendre son usage un peu
plus générique que de faire le site personnel d'iGor milhit, ce qui suppose
peut-être repenser complètement la page d'accueil. Mais voici ce qui est prévu
pour la [version
`0.2.0`](https://framagit.org/iGormilhit/portfoligor/-/milestones/2).
