---
title: "Un 4e live"
date: 2020-04-02T18:33:33+02:00
publishdate: 2020-04-02
draft: false
categories: ["musique"]
tags: ["streaming", "listening", "live"]
slug: un-4e-live
---

Mise à jour : le live a été mis en ligne sur
[https://id-libre.org/live#fourth](https://id-libre.org/live#fourth).

---

Depuis une année, trop rarement, je diffuse en streaming la musique que je joue
sur deux platines vinyles. Je me suis dit que je pouvais annoncer, lorsque je
me prends suffisamment à l’avance, les prochains *live*, parce que ce blog à
l’avantage incommensurable d’avoir un flux RSS.

Oui, parce que les *live* ont lieu sur [id-libre](https://id-libre.org), dont
les 3 pages d’accueil sont faites à la main, un jour peut-être ça sera un poil
mieux, mais pour l’instant, va falloir faire avec.

Comme on vit une période formidable, avec la personne qui arrive à vivre avec
moi, on s’est dit que ça nous ferait plaisir de partager avec des gens proches
ou pas proches un bref moment musical. On l’a déjà fait la semaine passée, le
28 mars 2020 et on **va le refaire ce samedi 4 avril 2020, à 17:30 (UTC+2)** et
ça se passera comme d’habitude sur
[id-libre.org/live](https://id-libre.org/live). Le live pourra
s’écouter directement sur la page web. Les plus curieux peuvent regarder dans
les sources le lien direct du stream pour le coller dans un VLC, par exemple.

Quand on joue avec les platines à deux, comme ça, on s’appelle *Radio relai*,
et chacune, chacun choisit 3-4 morceaux, ou pendant 15-20 minutes, ça dépend
des fois, et parfois on se permet de faire autrement. Bref, samedi, ça sera
*Radio relai*.

En général, je publie la liste des morceaux dans les jours qui suivent.
