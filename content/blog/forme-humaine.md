---
title: "Forme humaine"
date: 2020-03-10T17:05:23+01:00
publishdate: 2020-05-24
draft: false
categories: ["explorations", "musique", "traces"]
tags: ["rock", "hip-hop", "Casey", "histoire", "identité"]
slug: forme-humaine
---

De la suite dans les idées, et surtout tant pis pour les contraintes de
l'artgrobusiness, un cri jouissif dans le respect des traditions qui envoient
valser les lieux communs aussi increvables qu'à la ramasse, les trains à la
mode clignotent, alors que les piétons accumulent les tours du monde, c'est
dans les vieilles casseroles qu'on fait les ratatouilles les plus iconoclastes,
les pas de danse les plus insaisissables, les affirmations de soi les plus
libératrices. Tout est là. Faudra faire avec.

De toute façon, faut toujours faire avec. C'est l'étonnant mystère de la
liberté : cette joie. Ce n'est pas facile, faut pas croire dit-il, alors qu'il
se dévisse le doigt de son orbite oculaire, sort d'une léthargie temporaire
comme on pose le genou à terre, face à d'insolubles détails, spectres déjà
évaporés, effacés des mémoires sans histoire.

Faut dire qu'il y a des histoires qui réécrivent les mémoires, s'en foutent de
nos tombes outrées et c'est l'animal humain qui se réveille enfin, qui prend sa
revanche, enterré par une civilisation morbide, méprisante, pour tout dire dans
l'erreur.

{{< blockquote author="Ausgang, Casey" lang="fr" title="Chuck Berry dans Gangrène,* A Parté. 3760248832777. 2020" link="https://www.discogs.com/fr/master/view/1710013">}}
Et l'Occident qui avait honte a inventé le punk \
s'est haït lui-même
{{< /blockquote >}}

Trouver la porte de sortie en observant le chemin de celles et ceux qui
t'envoient balader, qui envoient balader les systèmes par brouettes entière,
par retour de charters. Prendre la balle à la volée, voler la balle à la
reprise, relever le défi et jouer la partie jusqu'au bout, déjouer les
héritages sans les renier, ne plus refouler ni rien ni personne, si ce n'est
les nostalgiques de l'immonde, les accrochés des cauchemars. Et dire, écrire
sans craindre la révélation, puisqu'il faut faire avec, faudra faire avec, le
seul moyen connu pour ne plus se prendre systématiquement les pieds dans les
pièges transmis de générations en générations, en finir avec ces besoins
d'images polies, repolies, dépolies qui ne servent qu'à se prémunir de vivre,
de risquer de vivre, de perdre patrie, patrimoine et son latin, mais retrouver
enfin forme humaine. Et danser.

{{< audio-cover src="ausgang-chuck-berry" img="casey-ausgang-gangrene.jpg" legend="Chuck Berry, d'Ausgang" >}}

---

<small>Le texte ci-dessus a été débuté peu après la sortie de l'album de
Ausgang et Casey intitulé *Gangrène*, que je trouve excellent, comme à peu près
tout ce à quoi participe Casey. Puis, il y a eu pas mal de perturbations, et
j'ai laissé ce texte de côté. Je l'ai repris récemment, chose peu habituelle.
Il faut croire que j'y tiens.</small>
