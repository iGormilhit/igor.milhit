---
title: "Mélancolie"
date: 2021-09-22T20:55:58+02:00
publishdate: 2021-09-25
draft: false
categories: ["traces"]
tags: ["vie", "mort", "mélancolie"]
postimage: melancolie.jpg
postimagedescription: Montagnes dans la brume.
slug: mélancolie
---

Une fois de plus, la mélancolie. Je ne veux pas savoir d'où elle vient. Le
fruit s'est présenté, que faire sinon le savourer, en jouir ? La mélancolie,
l'automne comme un fruit mûr, un pétale au sol, le jour plus court, la
fraîcheur bientôt froide. Les années passent. Des fêlures, des fissures,
miettes de murs, éclats de béton, peintures écaillées. Un décor sans
originalité, de générations en générations : mélancolie.

Quelle est douce ! Le souvenir de la pêche dans la vigne. Des braises dans le
foyer. Les vieux matelas sous les étoiles. Les mélèzes dans la nuit,
sentinelles ou bergers des siècles. Se tenir debout, comme on peut. Et observer
les mondes qui se disloquent. Les mondes qui apparaissent. Savoir qu'un jour on
ne saura plus suivre. Malgré tous les efforts.

Mélancolie. La vie chemine, serpente et danse. La mort rôde. La démesure tapie,
à l'affût, dans les détails du quotidien. L'essentiel imperceptible,
intangible. Tu ne le perçois pas, tu ne saurais le toucher, à peine le devine,
dérisoire avec tes trois mots mal arrangés. Il est là pourtant, tu le sais
bien, même sans le penser, ni pouvoir le nommer. Bien réel, il altère ce que tu
sens, interprète, construis.

Le chemin grimpe vers un col. Soleil ou pluie. Pour aller dans cette vallée-là,
impossible de ne pas quitter cette vallée-ci. Mélancolie.
