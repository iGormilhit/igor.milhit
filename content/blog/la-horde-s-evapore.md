---
title: "La horde s'évapore"
date: 2020-02-05T06:14:38+01:00
publishDate: 2020-02-10
draft: false
categories: ["traces"]
tags: ["lundi", "horde", "histoire", "mort"]
slug: la-horde-s-évapore
---

L'animal humain court dans la plaine, homme ou femme, se remarque la régularité
du rythme, ni rapide, ni lent, constant, le point de vue prend du recul,
l'angle s'ouvre, l'animal humain ne court pas seul, elle ou il, que vous
importe-t-il ?, est accompagnée, ce sont des membres de la horde qui parcourent
la plaine, foulent pieds nus la terre, soulèvent à peine la poussière, vers
l'est, le nord, le sud ou l'ouest, dans le courant de l'histoire, naissance
après mort, mort après naissance, de génération en génération, pour les siècles
des siècles, du paléolithique à l'anthropocène et ses piles au lithium, le
regard incarné s'offre l'illusion d'une accélération, l'espace resserré, le
tissu social couvre la steppe, phagocyte la vie sauvage, ronge les forêts,
assèche les zones humides, et cette filiation aux racines inversées plonge
dans l'infini cosmos des chromosomes, dans la suite forcément continue des
mères, des tantes, des sœurs, des filles, des pères, des oncles, des frères,
des fils, les frontières s'estompent, pastels frottés, lavis déteints,
parchemins grattés, des nouvelles chroniques, couches après couches, à chaque
foulée un monde complet est donné, s'épanouit et s'évapore, comme la horde dans
la tribu, la tribu, dans le peuple, le peuple dans les nations, les nations sur
les pistes célestes des longs courriers, des charters à touristes, à réfugiés
refoulés, et soudain, dans ce brouhaha, cette fourmilière, cette boîte de
Petri, un regard singulier, une lumière précise, une femme trace un chemin à la
machette, dans l'inconnu, dans ce qui ne peut se connaître et pourtant elle
nous l'éclaire, elle nous y reconnaît, va donc encore distinguer ce qui est
bénédiction ou malédiction.

Le petit trot des mots se suspend.

Le silence reprend sa place.

Yeux grands ouverts.
