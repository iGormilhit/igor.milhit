---
title: "Lundi, perturbations"
date: 2019-09-02T06:28:25+02:00
publishdate: 2019-09-03
draft: false
categories: ["traces"]
tags: ["lundi", "train", "perturbations"]
slug: lundi-perturbations
---

Se projeter dans les traces éphémères de l'avenir, même proche, constater que
les préparations échouent, sans régularité, fiables pourtant, que les
aiguillages s'égarent, t'égarent, compliquent encore un peu le sac de nœuds des
refus, des refoulés, des reconduites à la frontière, des émotions sans visas,
des cauchemars clandestins soignent la nuit, nettoient les connexions,
construisent le sens selon les plans au relief imprévisible, une boucle, une
roue, contre toute attente, ça avance, qui aurait pu en douter ? Train dérouté
en détresse dans le réseau resserré de mes tripes, la lumière bleue des
annonces de service clignote dans un monde qui refuse de voir, alors qu'aucun son
ne sort des haut-parleurs, de peur, de peur, de peur, de quoi on ne sait plus,
on ne veut pas savoir justement.
