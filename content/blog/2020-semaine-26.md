---
title: "2020, semaine 26"
date: 2020-06-28T16:55:11+02:00
publishdate: 2020-08
draft: false
categories: ["traces"]
tags: ["semaine", "pain", "basket", "coiffeur", "fermentation"]
slug: 2020-semaine-26
---

Peu ou trop de choses, cette semaine, selon l'angle, l'émotion ou la mesure. Et
tout ne peut être dit, du moins pas encore. Quel mystère, peut-être de le taire
tout à fait aurait été préférable.

[Jeanne][3] a acheté un ballon de basket, c'est amusant. Hormis quelques
paniers lors de l'été 2016, je n'ai pas touché ce type de balle depuis à peu
près 25 ans, et franchement, c'est plus tellement de l'âge de mes rotules.
Comme le contexte est surtout de prendre un peu de plaisir autour d'un panier,
j'ai tout le loisir de faire attention à mes genoux. Et après deux séances, ça
va déjà un peu mieux. Deux séances ? Oui, deux séances, d'à peine une heure,
mais elles m'ont remis un peu en mouvement, permis de partager un moment
sympathique, et c'est bienvenu.

J'ai aussi eu l'occasion d'aller me faire couper les cheveux, et ça faisait
bien longtemps -- depuis l'automne dernier, ce changement de tête me fait
également du bien, du même ordre que ces tirs au panier, une expérience de soi
qui se déplace, se décale. Tant mieux. J'ai également apprécié la discussion
avec la coiffeuse, qui m'a décrit ce que c'est de porter un masque toute la
journée, malgré la chaleur et le travail. Elle le porte aussi dans les
transports publics, donc de 07:00 à 20:00 presque sans discontinuer, ce qui
doit être en effet fatiguant. Merci donc, à toutes ces personnes qui font un
effort substantiel pour protéger les autres.

Récemment, on m'a signalé l'existence de [farines][1] particulières, qui ont
éveillé ma curiosité. J'ai notamment remarqué qu'ils vendent du pain pur
[engrain][2], qui selon toute apparence n'est pas réalisé au moule. J'ai donc
eu envie d'essayer avec la farine d'engrain de l'Abbaye de Presinge. Par
prudence, je suis parti sur une hydratation d'environ 63 %, et c'était une
bonne idée, le paton était très collant et dur à pétrir[^1], lui donner de la
structure a pris du temps et de la patience. Par contre, au pointage et à
l'apprêt, il a pris un volume appréciable. De même à la cuisson.

{{< figure src="/images/pain-engrain.jpg"
           caption="Pain d'engrain au sortir du four" >}}

Au final, je suis très content de cet tentative, le pain qui en a résulté est
(bientôt était) vraiment bon, avec une texture incroyable. Et j'en suis
particulièrement surpris, parce qu'on ne peut pas dire que j'ai géré la cuisson
très précisément. Parfois, le flou fonctionne aussi.

En ce moment, je suis en train de lire *Ni cru ni cuit*[^2], un livre sur les
aliments fermentés, d'un point de vue historique (et donc y compris la
préhistoire, aujourd'hui ça me semble étrange de vouloir parler de l'évolution
d'une chose aussi fondamentale que l'alimentation sans prendre un peu de
recul), mais aussi mythologique, symbolique, anthropologique et technique.
C'est passionnant, et très bien référencé[^3].






[1]: https://www.farinesdemeule.com/ "Les Maîtres de mon moulin, Cucugnan"
[2]: https://www.farinesdemeule.com/pains-d-engrain-petit-epeautre-26/pain-d-engrain-petit-epeautre-env-67.html "Le pain d'engrain des Maîtres de mon moulin"
[3]: https://id-libre.org/variations/

[^1]: J'utilise la technique expliquée dans cette vidéo de l'École internationale de boulangerie (EIDB) : https://www.youtube.com/watch?v=FkvjdqkZWqg

[^2]: FRÉDÉRIC, Marie-Claire, 2014. *Ni cru ni cuit : histoire et civilisation
de l’aliment fermenté*. Paris : Alma éd. ISBN 978-2-36279-107-9.

[^3]: J'aurais toutefois apprécié un système moins compliqué qu'une note en fin
d'ouvrage, contenant juste l'auteur et la date, qui renvoie à la bibliographie.
