---
title: "Ouvrir des possibles"
date: 2020-05-17T19:44:57+02:00
publishdate: 2020-05-17
draft: false
categories: ["explorations"]
tags: ["possible", "démocratie", "capitalisme", "écologie", "avenir"]
slug: ouvrir-des-possibles
---

J'avais ouvert la catégorie *[explorations](/categories/explorations)* en
constatant, comme beaucoup d'autres, qu'il était temps de passer à autre chose.
Ce *temps* dure depuis un certain temps, il prend racine bien avant ma
naissance, bien avant les chocs pétroliers et le début de la crise permanente,
caractéristique de la révolution conservatrice, ce baroud de déshonneur de la
société bourgeoise qui s'accroche désespérément à ses illusions, va pas
croire que c'est le seul groupe noyé dans l'illusion, il ne s'agit pas de
simplement, bêtement montrer du doigt l'autre, l'autre classe, mais il faut
bien dire qu'elle nous empêche puissamment de faire face à la situation. Bref,
il est temps, il est temps trop tard, mais qu'importe, la vie l'emporte,
l'engagement jusqu'au cou, que tu le veuilles ou non.

Il est temps de travailler pour essayer la démocratie, autre chose que cette
bouffonnerie qui ne sait que reproduire le même, essayer la démocratie qui
pourrait bien être l'anarchie, l'autogestion, vivre les rapports de force
autrement, s'éveiller à l'institutionnel et ne plus en être l'esclave aveugle.
Et d'un, de la démocratie.

Ce qui revient à déboulonner le capitalisme, ce truc qui nous a imposé un
système représentatif pour nous refuser la démocratie, justement. Sortir du
féodalisme, bien entendu, c'est bourgeois, mais la démocratie, non, pas les
classes populaires, pensez donc. Et surtout, ce système a toujours jalousement
gardé « l'économie » en dehors de toute décision politique. Mais ça suffit, ça
prendra le temps qu'il faut, le seul objectif dans ces matières désormais,
c'est se réapproprier le travail, les moyens de production, y compris la santé,
l'alimentation, le logement, l'investissement, rallonge la liste, n'aie pas
peur.

Enfin, et c'est essentiel, changer notre rapport au monde, au vivant, à
nous-mêmes. Ni le monde, ni le vivant, ni les animaux humains ne sont des
ressources que l'on peut piller, exploiter, épuiser (au sens de consommer,
exterminer). Et donc réfléchir à ce qui ouvre des possibles pour les vivants de
la Terre d'ici la fin du siècle. Qui est demain. Naître aujourd'hui, c'est
avoir 70 ans vers 2090, peut-être dans un monde avec une température moyenne
globale de 5 degrés Celsius supérieure à celle du début de l'ère industrielle,
[c'est-à-dire l'enfer, la famine, la
guerre](https://www.pnas.org/content/early/2020/04/28/1910114117). Et le climat
n'est qu'un des aspects de la crise monumentale que nous vivons.

Rien de nouveau sous le soleil, tu sais t'informer comme moi, peut-être mieux
d'ailleurs, tu sais agir comme moi, tu ne m'as peut-être pas attendu, puisque
des femmes et des hommes inventent des mondes possibles depuis des décennies,
me reste à faire ma part.

Pour le prochain billet, j'aimerais bien écrire sur l'état d'esprit que suppose
*ouvrir des possibles*, histoire de se débarrasser du nihilisme ambiant.

<small>S'il te venait l'envie de réagir à ce texte, comme aux autres
d'ailleurs, tu peux [m'écrire](mailto:igor+blog@milhit.ch), me parler sur [le
fediverse](https://pouet.it/@im), voire sur [framagit](https://framagit.org/iGormilhit/igor.milhit/), soyons fous.</small>
