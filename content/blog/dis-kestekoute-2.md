---
title: "Dis, Kestekoute 2"
date: 2021-02-15T10:27:11+01:00
publishdate: 2021-02-19
draft: false
categories: ["musique"]
tags: ["découverte", "kestekoute", "jazz", "M-Base Collective", "Steve Coleman"]
---

« Dis, qu'est-ce que t'écoutes en ce moment ? »

Alors que le train, ami délaissé, ami retrouvé, glisse sous le soleil de
février, me présentant au loin les Alpes et leurs pâturages d'avalanches
impartiales, la technique carbonée mais auxiliaire baigne mes percepteurs dans
une solution m-basique, musique à la complexité masquée, comme il se doit dans
nos temps pandémiques.

L'anatomie d'un *groove*, sans autopsie, sans dissection, le *loa* est bien
vivant, inaccessible au scalpel, se rit des règles parce qu'elles deviennent
lui et autre, mouvant, changeant, la pensée court, le souffle libre, déroule la
savane, la steppe, s'élance dans les vallées avec *homo erectus* et *homo
ergaster*, à chacune sa mesure, battre le temps, à contre-millénaire relire,
raconter l'histoire des femmes et des hommes que nous sommes depuis bien avant
n'importe quel déesse ou dieu.

Anatomie d'un groove. *Loa*nimal.

{{< audio-cover src="m-base-collective-nobody-told-me"
    img="m-base-collective-anatomy-of-a-groove.jpg"
    legend="Nobody Told Me by M-Base Collective" >}}
