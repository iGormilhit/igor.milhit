---
title: "2020, semaine 24"
date: 2020-06-14T08:45:35+02:00
publishdate: 2020-06-14
draft: false
categories: ["traces"]
tags: ["semaine", "lecture", "auto-hébergement", "pain", "écriture", "matérialisme"]
slug: 2020-semaine-24
---

## Introduction

Depuis quelques temps, je vois que blogueurs et blogueuses se mettent à faire
des résumés hebdomadaires, ou selon d’autres fréquences, de ce qu’elles et ils
font ou veulent faire. Parfois, j’y déniche des idées, des pratiques, des
inspirations, comme justement celle de me mettre, par ce mimétisme, peut-être
plus enrichissant qu’il n’y pourrait paraître, or je suis un primate qui aime
les moutons. Et puis, ça me rappelle les débuts des [*weblogs*][1] à la fin du
millénaire passé, qui n’étaient rien d’autre que cela.

## Informatique

Beaucoup de bidouilleries numériques cette semaine, grâce à dieu et à sa fête
[à la sortie de mon VPN professionnel][2], m’offrant un jour férié que j’ai mis
à profit pour, enfin, migrer *selam*, le serveur hébergé à la maison
([id-libre.org][3]), de debian stretch à buster. C’était aussi ce qui me
coinçait pour le suivi des mises à jour de *nexcloud*, à cause de la version de
{{< smallcaps "php" >}}[^1]. J’en ai profité pour installer et configurer
`zsh`, avec `oh-my-zsh` et `powerlevel10k` sur mes différents serveur. De même
avec la configuration de `vim` (ou `neovim` selon les cas). Des petits détails
qui me permettent d’avoir des habitudes similaires un peu partout.

J’avais aussi un souci avec ma configuration du serveur de mail sur *toumai*
(une machine chez Gandi, pour [igor.milhit.ch][4]), et aussi sur le serveur de
mail sur *selam*, que j’ai pu résoudre[^4]. Ce qui me donne envie de reprendre
calmement ces différentes configuration, maintenant qu’elles me font un peu
moins peur.

Cette semaine, j’ai aussi prêté un peu plus attention à la série de [billets
sur `vim`][5] de [@hyde~][6], ce qui a débouché sur l’obtention de *Practical
Vim*[^2]. Je le découvre à peine et j’ai déjà appris une méthode tout simple
pour avoir plusieurs configurations (grâce au paramètre `-u`), ce qui est bien
pratique pour dénicher un problème dû à une erreur de configuration.

## Lecture, divers et panification

Je continue la lecture du dernier Todd[^3], lentement mais sûrement. C’est
toujours un plaisir de le lire, parce qu’il est provocateur et polémique, mais
pas seulement, on apprend beaucoup et pour moi c’est une prise de conscience de
la mentalité étatique de la France, à commencer par ses classes sociales
supérieures, quel que soit le bord politique. Contrairement à Todd, j’en retire
l’idée que plus d’anarchie permettrait de s’organiser de manière plus humaine
et utile. Todd, lui, démontre que la voie étatique génère plus de
désorganisation et de destruction que d’ordre. Enfin, c’est ma lecture
totalement subjective.

J’ai repris un billet que j’avais commencé en avril, un billet qui cherche à
expliquer ce que j’ai compris d’une des variantes du matérialisme, au sens
d’explication du monde ou de fondement épistémologique, et pourquoi cette
compréhension du monde est source d’enchantement et de poésie, à l’inverse de
ce que l’on pourrait croire. Pour ma part, de plus, c’est lié à un certain
positionnement dont il a été question dans [*ouvrir des possibles*][7]. Au
cours de l’écriture, je suis reparti me documenter sur le web et j’ai
redécouvert la [version du matérialisme scientifique de Mario Bunge][8], qui
est justement celle qui me semble la plus élégante. Au cours de ces lectures,
je me suis dit que le sujet était bien entendu plus nuancé, fort heureusement,
que ce que j’en laissait transparaître. Aussi, je pense laisser ce texte mûrir
au soleil du doute et du questionnement.

Enfin, cette semaine j’ai fait et suis à nouveau en train de faire du pain, une
fois près de 2 Kg, avec un des deux pains en cadeau de bienvenue à des voisins
qui rentrent d’une années sabbatique à l’étranger, année et retour un peu
mouvementés par la pandémie actuelle. Un peu de bon pain au levain, ça ne peut
que faire du bien. Et la deuxième fournée comprend également un pain à donner,
à une amie cette fois.

C’est tout pour cette semaine, du moins dans ce billet qui est déjà plus long
que raisonnable. Peut-être à la semaine prochaine.




[^1]: À noter que je n’aime pas trop jouer avec les versions de PHP en
utilisant des dépôts alternatifs, ce qui m’a donné du fil à retordre dans les
migration de version de debian, par le passé.

[^2]: NEIL, Drew, 2015. *Practical Vim: edit text at the speed of thought*. Second edition. Raleigh, North Carolina : The Pragmatic Bookshelf. The Pragmatic Programmers. ISBN 978-1-68050-127-8.

[^3]: TODD, Emmanuel, 2020. *Les luttes de classes en France au XXIe siècle*. Paris : Seuil. ISBN 978-2-02-142682-3.

[^4]: Peut-être que je ferai des courts billets spécifiques, mais je ne veux
pas m’étendre ici.

[1]: https://fr.wiktionary.org/wiki/weblog "Définition sur le wiktionary."
[2]: https://www.valais.ch/fr/activites/culture-patrimoine/traditions/fete-dieu "La Fête-Dieu sur le site du tourisme valaisan."
[3]: https://id-libre.org
[4]: https://igor.milhit.ch
[5]: http://lazybear.io/posts/2020-03-07_vim-weekly-tips/ "Premier billet de la série, en anglais."
[6]: https://lazybear.social/@hyde "Son profil sur le fediverse."
[7]: /ouvrir-des-possibles
[8]: https://fr.wikipedia.org/wiki/Mat%C3%A9rialisme_scientifique#Le_mat%C3%A9rialisme_scientifique_de_Mario_Bunge "Section d'article wikipedia en français."
