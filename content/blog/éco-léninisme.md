---
title: "Éco-léninisme"
date: 2021-11-19T18:41:50+01:00
draft: false
categories: ["explorations"]
tags: ["écologie", "politique", "climat", "Fanon", "décolonisation"]
slug: éco-léninisme
---

{{< blockquote author="Andreas Malm" title="L’urgence climatique rend caduc le réformisme" link="https://www.revue-ballast.fr/andreas-malm-lurgence-climatique-rend-caduc-le-reformisme/" lang="fr" >}}
Il faut mobiliser [l’héritage de
Fanon](https://www.revue-ballast.fr/labecedaire-de-frantz-fanon/ "L'abécédaire
de Frantz Fanon sur Ballast.") dans le mouvement écologiste. Fanon pense
l’usage de la violence dans un autre contexte, celui de
la colonisation. Il analyse la situation qui précède les luttes de
décolonisation ainsi : les gens souffrent en silence et intériorisent
l’oppression, ce qui génère du désespoir et des troubles psychiques. Lorsqu’ils
s’émancipent des colons par la lutte armée, il se produit une libération
psychologique, une réhabilitation mentale. Il existe des similitudes avec les
injustices environnementales. Le désespoir est le sentiment le plus largement
partagé devant l’urgence climatique, ce qui engendre des déséquilibres
psychologiques réels. La seule solution pour rompre avec ce cercle vicieux est
l’action collective, grâce à laquelle ces sentiments se retournent contre les
causes du phénomène.
{{< /blockquote >}}

Transposer Fanon dans d‘autres contextes, ici les catastrophes écologiques qui
nous entourent et prennent de l‘ampleur, me redonne de l'énergie. Cette
transposition en suggèrent d‘autres, c'est pas les fronts qui manquent. Ce qui
manque, c‘est l'engagement, l'engagement dans le travail collectif. Travail
oui, parce qu‘il va bien falloir mettre les mains dans le cambouis.

{{< blockquote author="iGor milhit" link="https://pouet.it/@im/107303855283685874" lang="fr" >}}
En découdre ?

Va peut-être falloir s‘y résoudre.
{{< /blockquote >}}

~~Sur un autre plan, je vais surtout devoir me bidouiller de quoi (un
[*shortcode*][2]) faire une citation correcte, le simple `blockquote` de
[markdown][3] n‘est pas satisfaisant.~~ Fait.

[1]: https://www.revue-ballast.fr/labecedaire-de-frantz-fanon/ "L'abécédaire de Frantz Fanon sur Ballast."
[2]: https://gohugo.io/content-management/shortcodes/
[3]: https://fr.wikipedia.org/wiki/Markdown "Article Wikipédia en français."
