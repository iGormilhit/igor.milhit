---
title: "portfoligor v0.2.0"
date: 2021-11-04T09:26:09+01:00
draft: false
categories: ["publication numérique"]
tags: ["thème", "portfoligor", "version", "hugo"]
postimage: "portfoligor-v020.png"
postimagedescription: "Capture d'écran du dépôt sur framagit"
slug: portfoligor-v0.2.0
---

[*portfoligor*][theme], le thème pour {{< smallcaps "hugo" >}} que je bidouille
pour ce site, a été publié en version `v0.2.0` :

- La [publication][v020] elle-même.
- La [liste des changements][changelog].

Parmi les améliorations notables, le [*README*][readme] est enfin à peu près
complet et permet (presque) de comprendre comment utiliser le thème. Non pas
qu'il soit si compliqué que ça, mais s'il faut lire le code des *templates*
pour savoir comment le configurer, ce n'est pas terrible. Il reste un peu de
boulot de ce côté, mais c'est un début.

Plus concrètement, côté interface, l'affichage des métadonnées des billets de
blog a été améliorée. Ces informations sont désormais dans une petite section
(*Informations sur le billet*) qui est par défaut réduite et qui peut s'étendre
facilement pour montrer la date de publication, de mise à jour éventuelle, les
catégories, les étiquettes et le lien permanent. Pour les pages statiques, un
paramètre peut être ajouté dans le *front matter* de la page (`layout:
static`), afin de ne pas afficher ces informations.

Toujours côté métadonnées, elles sont désormais étendues, afin non seulement
d'être utilisable par des outils de gestion de référence comme Zotero, mais
aussi par les médias sociaux. Une image et sa description peut être définie au
niveau du site lui-même ou pour chaque page ou billet de blog. Cette image et
sa description sont affichées au sommet des billets de blog et des pages
statiques.

{{< figure src="/images/integration-reseaux-sociaux.png"
           caption="Exemple d'intégration dans un réseau social"
           link="https://pouet.it/@im/106917930375991977">}}

Le menu principal et celui qui est dans le pied de page sont configurables au
niveau du site lui-même, alors que jusqu'ici il fallait éditer le thème pour
pouvoir les modifier. Dans le pied de page, une entrée a été ajoutée qui pointe
vers la dernière modification de la page visitée directement dans les sources
(le *commit*).

Notamment dans le but d'améliorer l'affichage de menus avec plus d'entrées,
l'apparence générale du thème a été modifiée. Il s'agit d'une simplification, à
la fois au niveau de la structure et du rendu, ce qui devrait rendre le tout un
peu plus lisible et facilite l'adaptation aux diverses tailles d'écran.

Surtout, cette simplification prépare le terrain pour [la
suite][milestone030] ! Des remarques, des suggestions ? N'hésite pas à [me
contacter][contact] ou a ouvrir une [*issue*][issue].

[v020]: https://framagit.org/iGormilhit/portfoligor/-/releases#v0.2.0
[changelog]: https://framagit.org/iGormilhit/portfoligor/-/blob/dev/CHANGELOG.md
[readme]: https://framagit.org/iGormilhit/portfoligor/-/blob/dev/README.md
[milestone030]: https://framagit.org/iGormilhit/portfoligor/-/milestones/4
[theme]: https://framagit.org/iGormilhit/portfoligor/
[contact]: http://localhost:1313/#online
[issue]: https://framagit.org/iGormilhit/portfoligor/-/issues/new?issue%5Bmilestone_id%5D=
