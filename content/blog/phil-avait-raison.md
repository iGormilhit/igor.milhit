---
title: "Phil, tu avais raison"
date: 2019-10-07T06:43:26+02:00
publishdate: 2019-10-14
draft: false
categories: ["traces"]
tags: ["Phil", "deuil", "mort", "vie"]
slug: phil-tu-avais-raison
---

Tu me l'avais dit plusieurs fois, désormais l'avenir allait nous réserver plus
de mauvaises surprises que de bonnes. Sans pouvoir te donner tort, j'ai
à chaque fois cherché à dénicher ne serait-ce qu'un peu de lumière dans cette
promesse, à travestir le réel. La dernière fois, c'était quelques semaines
après le décès de ma grand-mère, la dernière représentante de l'étage
*grands-parents* de ma fusée existentielle. La dernière fois, c'était quelques
heures avant ton propre décès, par défaillance du cœur, quelques minutes après
m'avoir appelé, m'avoir demandé conseil, demandé de l'aide.

…

Depuis, ça ne s'est pas vraiment arrêté. Et à ces bonnes nouvelles, s'ajoutent
des histoires qui n'ont aucun sens, qu'on ne peut décrire sans faire d'erreur.
J'avoue que ta droiture me manque. Je suis un peu perdu sans ton nord.

C'était facile de te dire que malgré les années, l'âge et sa logistique de
grincements, de douleurs dorsales, son train fantôme, que malgré les fantômes
on allait miner du plaisir dans le réel ou dans notre perception du réel. Voici
venu le temps de le mettre en pratique et j'avoue que je me sens moins stable
sur le sol, les pieds moins assurés. Je ne pense pas que la tâche soit
irréalisable, les primates ont des ressources surprenantes, mais j'éprouve un
certain vertige, après ces événements qui m'ont fait sonder une fois de plus le
vide.

Parce que le vide est sans mesure, insondable. Permanent.
