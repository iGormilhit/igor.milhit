---
title: "Tristesse"
date: 2021-11-07T20:37:18+01:00
draft: true
categories: ["traces"]
tags: ["tristesse", "automne", "web", "point de vue"]
slug: tristesse
---

Alors que le ciel tient la grisaille en équilibre, un arbre dépose dans un coin
du parc des éclats trop rouges pour être honnêtes. « L‘automne m'a tuer ».
Doucement, lentement, presque en silence, en masse, à grande échelle. Rien à
faire. S‘entêter à s'extasier. S'agripper à nos certitudes : et si le printemps
ne revenait pas ?

Non, ce n‘est pas ça. Plus ça. Je l'ai perdue cette émotion vieille de 36
heures. Et chaque émotion, bien que se déployant dans des structures cérébrales
similaires, est unique. Ce qui revient à dire qu‘elle n'est pas véritablement
descriptible. Ni communicable. On fait avec, depuis des centaines de
millénaires.

L‘autre jour, je me disais que des [bouts de web][dav] [existent encore][hugo],
et que les mots des autres, leurs pérégrinations, leurs explorations me
font du bien, m‘aident à ne pas me laisser flétrir, à me donner encore envie
d‘essayer de dénicher un autre angle, donner la chance à un autre point de vue,
une autre fêlure, une autre possibilité. Plutôt que le mur des impossibles que
je construis presque par réflexe. S‘enfermer, en sécurité, loin du danger de
ces printemps qui ne cessent de revenir.

Bizarrement.

[dav]: https://larlet.fr/david/
[hugo]: https://hugo.soucy.cc/
