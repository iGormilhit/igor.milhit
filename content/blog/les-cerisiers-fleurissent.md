---
title: "Les cerisiers fleurissent"
date: 2020-03-18T12:38:18+01:00
publishDate: 2020-03-21
draft: false
categories: ["traces"]
tags: ["#ecritHebdo", "écriture", "enfermement", "sortie"]
slug: les-cerisiers-fleurissent
---

« Ah tiens, les cerisiers fleurissent. »

Il veut sortir. Aller se coucher dans l'herbe, sentir sa peau chauffer au
soleil de mai. Enfin, d'avril. Non, de mars. Peu importe. Il veut aussi jouer
avec des enfants, les prendre dans les bras, rigoler, se casser la gueule en
trébuchant, jouer à l'élastique, oui, même à l'élastique, ça serait
fantastique, et puis faire une sieste auprès du grand-père, sur l'alpage,
à l'ombre des mélèzes qui ont survécu à la grippe espagnole qui venait de
Chine, si ça se trouve c'était encore à cause des Américains, bref, retrouver
le monde d'avant, où l'avenir était infini, une sorte de maintenant éternel,
juste avec le rythme lent des saisons, de son âge qui bouge, savoir chaque jour
faire des trucs en plus, retrouver ce paradis duquel on se fait jeter avant
même d'avoir compris que c'était le paradis, perdu, parce qu'il ne peut y avoir
de paradis qui ne soit d'abord perdu.

Il veut sortir. Marcher dans la ville, se fondre dans la foule, s'y sentir seul
dans sa bulle musicale, danser mentalement grâce à la marche qui s'accélère ou
ralentit selon le rythme de la musique, visiter les murs comme on visite un
musée, avec ses déclinaisons de tags rageurs ou poétiques, rageurs et
poétiques, rageurs tout court, respiration indispensable. Ou pédaler, slalomer
entre les cubes d'acier et de plastique presque immobiles, se griser de vitesse
quand c'est possible, et s'essouffler dans les montées, pouvoir se transporter
tout seul, plus loin, jusqu'à sortir de la ville, retrouver un horizon plus
large, gonfler ses poumons des immenses nuages où se peignent les couchers de
soleil, où s'annoncent les orages qui réjouissent l'âme et le corps, enfin,
l'âme ou le corps, deux mots pour la même chose. Bref, il veut sortir.

« Ah tiens, les cerisiers fleurissent. »

Il a déjà connu une sorte d'enfermement, à l'époque ça avait duré plusieurs
années et c'était tombé sur lui. Enfin, il n'en sait rien, ça avait bien dû
tomber sur quelques autres aussi, lorsque tu penses être seul,
vraisemblablement tu es en réalité plusieurs millions, fourmilière humaine
oblige. Et donc, c'était tombé sur lui. Une manière d'éviter d'écrire qu'il
se l'était imposé, parce que d'un côté personne ne l'avait forcé, pas même lui
justement. Peut-être aurait-il pu faire autrement, mais ça n'aurait pas été
possible, il veut sortir de cette aporie, lorsqu'il n'y a pas de solution, il
n'y en a pas. S'enferrer à chercher des causes premières et des motivations,
c'est être réduit à ratiociner, vain blabla. C'était tombé sur lui. Il aurait
bien voulu sortir, se sortir de là, mais il ne savait pas comment faire, si
c'était permis, pourquoi ça ne l'aurait pas été, alors il avait simplement
pillé la bibliothèque publique, cherché à comprendre le monde, parce que
celui-ci semblait s'être trompé d'époque, obstiné à vivre comme dans les heures
les plus sombres du 19<sup>e</sup> siècle européen, alors qu'on s'éclatait en
plein anthropocène post guerre froide. La confusion était totale. Et écouter de
la musique, en buvant du café, en fumant des joints, en boucle : livres,
musique, café, joints. Pendant une petite décennie.

Et il était sorti.

« Ah tiens, les cerisiers fleurissent. »

Ça avait été les retrouvailles avec la lumière, le souffle, la distance,
l'horizon ouvert, et les nuages, les nuages parce qu'ils donnent la mesure de
l'immensité du ciel, un contraste frappant avec les détails du faux crépi du
mur de sa chambre, des fleurs, des odeurs, de la poussière, la pluie, l'orage,
la joie du monde, pouvoir à nouveau fatiguer ses muscles, redonner de l'espace
aux poumons, passer de mort vivant à vivant vivant. C'était inespéré. Un moment
à nul autre pareil. Parvenir au sommet de la montagne.

Ce sommet est une illusion d'optique, l'épaule de l'arête, le chemin pierreux
monte encore vers l'épaule suivante, et d'épaule en épaule il finit par ne plus
bien savoir si ça monte encore, si l'espace ne s'est pas mué,
imperceptiblement, en une boîte qui se resserre, tous ces « si » s'évanouissent,
l'évidence ne peut plus être ignorée, une nouvelle sortie devient nécessaire,
à trouver, à creuser, à déchirer, à dessiner, à esquisser, à chaque fois il
s'agit d'être un peu plus malin, de faire en se laissant faire… Il se sent
à court d'imagination. S'interroge sur la pertinence des routines
d'assouplissement. Il veut sortir.

« Ah tiens, les cerisiers fleurissent. »

Se faire fleur de cerisier, de prunier, d'amandier, le particulier est un
détail, charmant, essentiel même, mais un détail. Se faire fleur. Et sortir,
sans même l'avoir voulu. Parce que. Parce que quoi ? La mère et le grand-père
partent d'un grand éclat de rire. Il va devoir faire avec, se contenter de
cette réponse, jongler avec les problèmes sans solution, les laisser se briser
au sol, comme de petites fioles libérant des parfums dont il se souvient bien,
mais ne sait, comme toujours, nommer.

----

<small>L'idée de ce texte vient de ce message :
https://imaginair.es/@Ezelty/103843501519501506. \
Des textes d'autres personnes peuvent être cherché au bout des liens suivants,
parmi d'autres : [https://imaginair.es/tags/EcritHebdo](https://imaginair.es/tags/EcritHebdo),
[https://pouet.it/tags/EcritHebdo](https://pouet.it/tags/EcritHebdo).</small>
