---
title: "Mardi, mouvement"
date: 2020-01-21T06:06:38+01:00
publishDate: 2020-01-21
draft: false
categories: ["poésie"]
tags: ["lundi", "mouvement"]
slug: mardi-mouvement
---


Dans un sens, ou dans un autre, vertical ou horizontal, peut-être de biais, le
long d'une traverse, des sinuosités d'une diagonale, à une échelle réduite, le
règne d'une boucle, un cycle court, une répétition, le même succède au même,
afin d'aller voir plus loin, parce que l'inconnu peuple les territoires
cartographiés, l'érosion dessine des terres renouvelées, les chemins n'ont pas
dit leur dernier mot avant le dépassement des frontières de l'espoir et de
l'angoisse, avant la désillusion des faits, l'irréductible imperfection,
peut-être mon plus précieux salut, l'inachevé, le bancal, une fissure dans la
faïence, une transparence dans le granit, la plaine nocturne et sombre
accueille des forêts d'émotions en boucle, répétition, même, avec des
variations subtiles, matières à modeler, à sculpter, à polir si le temps le
permet, si je me permets de prendre le temps, de frotter mes désirs et mes
peurs aux rugosités des contraintes, des contrariétés, d'aller fouiller dans
mon coffre aux trésors, babioles et verroteries, le butin des rencontres,
à l'assaut du *flying dutchman*, traversées du désert, explorations du
*maelström* et s'en tirer avec quelques perles de verre, billes ébréchées,
utiles pourtant pour suivre la dérive du nord et déjouer les pièges tapis en
pleine lumière sans lesquels le jour nouveau ne saurait advenir, avec son
aigreur et son ivresse déjà passée, amortie par l'accoutumance, engourdissement
de l'habitude de vivre, un pas, puis l'autre, sous l'enclume solaire, face au
mur des vents, la pluie, la neige, le gel, se porter à la rencontre du confort
et de l'inconfort, cet équilibre qui n'en finit pas de se perdre, tant qu'il
y a encore quelque chose à perdre…

Tant qu'il y a encore quelque chose à perdre.
