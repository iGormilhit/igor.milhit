---
title: "Lundi, un mardi comme un autre"
date: 2019-12-10T06:10:45+01:00
draft: false
categories: ["traces"]
tags: ["lundi"]
slug: lundi-un-mardi-comme-un-autre
---

De retour dans cet espace, source contrainte de liberté, en mouvement, en
trajet, en chemin, ces bons vieux rails, les voies de chemin de fer qui
signalent ce pays mieux que les vaches ou les véhicules d'évasion fiscale, mon
histoire, l'histoire de la plupart des habitants de ce pays, un espace entre
`2,33` lieux, perdant sa course sur le fil du temps qui serpente, couleuvre au
travers du lac, gomme à frontière. De retour, parce que la contrainte que l'on
se choisit est une discipline et je suis l'éternel indiscipliné, coincé
volontairement sans volonté dans des boucles à l'infinie routine, routinière
douceur, une vieille fuite par résistance passive, offrir un peu d'inertie au
tumulte, mais ça, c'était avant et jusqu'à quand vais-je rester englué dans ce
maintenant dépassé, inactuel ?

Comme l'eau, plus précisément comme la *goutte* d'eau, une part infinitésimale
à chaque fois, puisque la seule promesse tenue est celle de l'échec, cet échec
sans honte, à l'obscurité lumineuse, terre ni connue ni inconnue, mystère sans
secret ni sens caché. Résistance passive, ou sidération face à l'inéluctable,
le savoir sans réellement parvenir à y croire. Et pourtant, persister dans
l'imperfection, encore un peu, un peu plus, comme on marche, comme on prend le
train.

Trains en marche, sans grève, quelques travaux, des accidents de personne, des
retards statistiquement significatifs de `3'17''`, des gares croissent autour
des cités dortoirs sans histoires, ici rien ne se passe, ou plutôt rien
n'atteint la chronique consensuelle, la domination est si bien établie que la
classe médiocre se croit aux commandes puisqu'elle est encore servie par les
prolétaires, souvent étrangers de générations en générations, ah ce petit
bonheur de glisser sans aspérités, pourvu que rien ne vienne perturber notre
âme d'huile, oh, à peine quelques verres d'eau tempétueux dans le secret de
l'urne, le plus souvent contre ces prolétaires qui de générations en
générations oseraient prétendre qu'ils sont la Suisse, qu'ils sont justement
notre prétendue valeur suprême, le travail, alors que nous ne sommes que des
pilleurs discrets, laborieux, efficaces…

On sent comme un courant d'air, la possibilité d'une lumière, comme si notre
Mur, notre Sarcophage, notre bulle de béton se fissurait enfin.
