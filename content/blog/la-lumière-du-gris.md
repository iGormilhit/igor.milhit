---
title: "La lumière du gris"
date: 2019-08-20T16:59:40+02:00
publishdate: 2019-08-21
draft: false
categories: ["traces", "poésie"]
tags: ["fatigue", "gris", "lumière", "pluie", "gratitude"]
slug: la-lumière-du-gris
layout: verse
---

la grisaille se décline en cohérences \
dégradés homogènes embrassent les cormorans \
ne sèchent pas leurs ailes sous la pluie \
seul un village brise la perfection \
elle lave l'été délave le soleil \
coche les tâches trempe les agendas du web \
lavis numériques les uns et les zéros \
bavent les uns sur les autres \
on se risquerait à croire au salut \
on se contente de dire bonjour au destin \
qui joue à se tromper de train \
sans y avoir l'air sans dés ni rien \
enverra une note de frais à nostradamousse inc. \
tout plutôt que de s'échiner encore \
à caser les heures dans les jours dans les semaines \
mais marcher pieds nus sur le bitume \
sous la pluie pour se laver de la rumeur managériale \
merci la vie merci la météo les masses d'air \
pour vos asphyxies et vos morsures, j'en pleurerais \
de gratitude \
sans explication oui gratuitement \
l'émotion n'a aucun sens, elle est le sens \
et c'est beau parce que c'est absurde \
une fraternité inexplicable tangible \
et quoi d'autre ?
