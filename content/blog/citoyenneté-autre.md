---
title: "Citoyenneté autre"
date: 2021-11-29T11:21:54+01:00
draft: false
categories: ["explorations"]
tags: ["Friot", "communisme", "démocratie"]
slug: citoyenneté-autre
---

{{< blockquote lang="fr" author="Bernard Friot"
               title="En travail : conversations sur le communisme, p. 53"
               link="https://ladispute.fr/catalogue/en-travail-conversation-sur-le-communisme/" >}}
Or, c'est la production dans son entièreté qui doit faire l'objet d'une
décision et d'une réalisation démocratiques.

Cela suppose d'en finir avec l'impôt […] au bénéfice d'une socialisation
intégrale des valeurs ajoutées dont je viens de rappeler qu'elle peut s'appuyer
sur la cotisation sociale interprofessionnelle à taux unique du régime général,
prémisses communistes à généraliser. […] <mark>Dans le mouvement communiste,
être citoyen, ça n'est pas « payer ses impôts », c'est définir et assurer la
production de valeur ajoutée. Et donc assumer la responsabilité de toutes les
institutions de la valeur.</mark> {{< /blockquote >}}

J'ai commencé récemment le livre des conversations entre Bernard Friot et
Frédéric Lordon. Contrairement à ce qui se passe le plus souvent lorsque
s'organisent des rencontres de ce type, l'échange est particulièrement fertile et
épanouissant. Aussi, des citations à faire, il y en a un bon nombre, même
après moins de 100 pages.

Celle-ci m'intéresse particulièrement parce qu'elle permet de bien sentir à
quel point économie et politique sont liées, et à quelle échelle la proposition
de Friot et de [réseau salariat][1] est émancipatrice. Le salaire à vie, c'est
d'abord ça, devenir collectivement souverain sur le travail, sur l'ensemble de
la production. C'est non seulement émancipateur, mais c'est également la bonne
manière de faire face aux catastrophes écologiques et climatiques qui sont en
cours.

Il ne s'agit plus de quémander un emploi pour avoir le droit de valoriser du
capital, et de payer des impôts pour en déléguer la gestion à un personnel
politique et une aristocratie de grandes et grands fonctionnaires. Il s'agit de
décider ensemble du niveau de salaire et de ce qui doit être produit, et
surtout comment ça devrait l'être.

Bien sûr, ça ne règlerait pas tout d'un coup de baguette magique, mais dans ce
contexte, les *COPmachins*, ça serait nous, nous toutes et tous. Un sacré
foutoir, certainement, mais autre chose que des cravates et des tailleurs
baladés en jets privés et qui blablatent dans le vide. Ce qui voudrait dire
aussi qu'on ne pourrait plus se limiter à se lamenter de nos élites (qui en
réalité ne sont pas *nos* élites, mais les représentantes et représentants de
la Bourgeoisie) : nous serions enfin responsables, adultes quoi. Et *avec* Greta,
pas *contre* elle.

La référence exacte :

{{< smallcaps "Friot" >}}, Bernard et {{< smallcaps "Lordon" >}}, Frédéric,
2021. *En travail : conversations sur le communisme*. Paris : La Dispute.
Entretiens. ISBN 978-2-84303-322-3.


[1]: https://www.reseau-salariat.info
