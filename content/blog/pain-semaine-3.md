---
title: "Pain de la semaine (3)"
date: 2022-01-02T15:45:22+01:00
draft: false
categories: ["faire des trucs"]
tags: ["pain", "Perséphone", "levain"]
slug: pain-semaine-3
postimage: "persephone-2022-01-02.jpg"
postimagedescription: "Portrait d'une boule de pain aux grignes en damier, posée en biais dans son banetton."
---

Pain de la semaine, ou les aventures de Perséphone.

Farine(s) : ⅕ rouge de Bordeaux, ⅘ pétanielle noire de Nice. \
Hydratation : Environ 68 g d'eau pour 100 g de farine. \
Fermentation : Courte, 3 heures et demi d'apprêt, 1 heure et demie de pointage. \
Poids final : 890 g.
