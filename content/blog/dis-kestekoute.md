---
title: "Dis, kestekoute ?"
date: 2021-01-30T13:02:53+01:00
draft: false
categories: ["musique"]
tags: ["découverte", "kestekoute", "afrobeat", "Kokoroko"]
slug: dis-kestekoute
---

« Dis, qu'est-ce que t'écoutes en ce moment ? »

En ce moment, comme toujours, j'écoute pas mal de choses différentes, notamment
un disque de [Beans][1] qui sait soigner ton âme absente, à sa manière décalée,
traçant des pistes au travers d'univers encore non répertoriés. Mais, en ce
moment, il y a un disque qui me parle tout particulièrement, un groupe
d'[afrobeat][2] *British*, mené par une section cuivre entièrement féminine.

De leur premier disque, tout est bon, mais je voudrais mettre en évidence le
dernier morceau, parce qu'il est doux comme un refuge dans la tempête, comme
une île à l'abri de la houle, comme une main posée sur l'épaule, avec douceur,
une absence de mot qui dit ce qui ne peut l'être, la possibilité de poser son
sac, de faire une pause, cesser de réprimer des sentiments peut-être trop
lourds, d'avoir des espaces libres dans un texte pour y mettre ce qu'on veut,
et même pour rien n'y mettre.

En l'écoutant, ce morceau, je pense à quelques personnes, à pas mal de
personnes en réalité, et il y a toutes les personnes auxquelles je ne pense
pas, que je ne connais pas, peu importe, quelle générosité de la part de
Kokoroko de nous offrir ce moment qui répare.

Ah, sur [leur site][3], on apprend que *Kokoroko* est un mot Orobo, un peuple
et une langue du Nigeria, qui signifie « soit fort·e ! ».

{{< audio-cover src="kokoroko-abusey-junction"
    img="kokoroko.jpg"
    legend="Abusey Junction par Kokoroko" >}}


[1]: https://en.wikipedia.org/wiki/Beans_(rapper) "Article Wikipedia en anglais"
[2]: https://en.wikipedia.org/wiki/Beans_(rapper) "Article Wikipedia en français"
[3]: https://kokoroko.bandcamp.com/album/kokoroko
