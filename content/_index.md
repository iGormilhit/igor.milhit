---
title: "Accueil"
date: 2019-03-14T06:32:39+01:00
draft: false
---

## À la ville…

Vieux comme la [crise du pétrole][cp], je suis né entre des livres, des taches
d‘encre et quelques [vinyles][vinyl]. Les noyaux des centrales nucléaires se
sont [mis à fusionner][wpfusion], les empires qui suivirent la fin des empires
[s‘écroulèrent][wpurss], s‘écroulent encore, et à la croissance des montagnes
de papiers s‘ajoute la croissance des montagnes de déchets électroniques. Au
milieu de cette dématérialisation joyeuse, j‘observe les [traces
éphémères][blog] de mon cheminement chaotique.

## Comme au bureau ?

### Aujourd'hui

Depuis <span itemprop="startDate">avril 2022</span>, je travaille comme
<span itemprop="hasOccupation">bibliothécaire spécialiste</span> pour la
discipline [*médecine clinique*][mc] à la
<span itemprop="workFor">bibliothèque de l'Université de Genève</span>, sur le
site du *Centre médical universitaire*. Et c'est avec un grand plaisir que je
retrouve des activités plus proches des personnes utilisant les services d'une
bibliothèque, que ce soit à l'accueil, pour la gestion des collection et dans
le cadre de formations.

### Jusqu'ici

De métier, je suis <span itemprop="jobTitle">*Spécialiste {{< smallcaps "hes"
>}} en information documentaire*</span>, ce que je traduirais volontiers par
*artisan en sciences de l’information*, afin de souligner que les travailleurs
et travailleuses *incarnent* des savoir-faire construits patiemment. Plus
précisément, la filière que j‘ai suivie formait des archivistes,
bibliothécaires et documentalistes. Une formation polyvalente donc, enrichie
par des méthodes et des outils numériques, que ce soit pour fabriquer des
documents, structurer de l‘information ou essayer de travailler à plusieurs, ce
qui n‘est guère nouveau dans l‘histoire des [espèces humaines][homo], mais qui
est à la fois facilité et rendu complexe par la numérisation du monde, ou plus
modestement d‘une partie de celui-ci.

Une [fameuse vidéo][minitel2.0] et un intérêt marqué pour l’[informatique
libre][libre] ont orienté ma curiosité vers des champs particuliers, mais en
réalité centraux dans notre paysage passablement webocentré : le web, les
serveurs, l‘auto-hébergement (au moins partiel), les {{< smallcaps "sigb" >}} ,
les {{< smallcaps "cms" >}}, les [fabriques de publication][fabriques], le
format de balisage léger markdown, LaTeX, pandoc, etc.

Ces orientations m‘ont amené à rejoindre
de <span itemprop="startDate">2016</span> à <span itemprop="endDate">2021</span>
l‘équipe <span itemprop="workFor">[{{< smallcaps "rero+" >}}][rero+]</span>,
afin de participer aux projets [rero21][rero21], à savoir
[{{< smallcaps "rero ils" >}}][reroils], un système de gestion de bibliothèques
et de réseaux de bibliothèques et [{{< smallcaps "sonar" >}}][sonar], un
service d‘archives ouvertes, les deux sous licence AGPL. J‘y ai participé
essentiellement comme *<span itemprop="hasOccupation">scrum master</span>*,
mais également pour l‘élaboration des spécifications du point de vue métier,
ainsi qu‘à la coordination avec les bibliothécaires des clients et partenaires
de {{< smallcaps "rero+" >}}.

[cp]: https://fr.wikipedia.org/wiki/Premier_choc_pétrolier "Article Wikipedia sur le Premier choc pétrolier"
[vinyl]: https://www.discogs.com/fr/user/ignami/collection?sort=artist&sort_order=asc "Mon compte Discogs"
[wpfusion]: https://fr.wikipedia.org/wiki/Fusion_du_c%C5%93ur_d'un_r%C3%A9acteur_nucl%C3%A9aire#Cas_r.C3.A9els "Section Cas réels de l'article Wikipedia sur la Fusion du coœur d'un réacteur nucléaire"
[wpurss]: https://fr.wikipedia.org/wiki/Union_des_r%C3%A9publiques_socialistes_sovi%C3%A9tiques#Derni.C3.A8res_ann.C3.A9es_de_l.27URSS_.281985-1991.29 "Section Dernières années de l'URSS de l'article Wikipedia sur l'URSS"
[blog]: /blog
[pandoc]: https://pandoc.org/ "site du logiciel Pandoc"
[libre]: https://fr.wikipedia.org/wiki/Logiciel_libre
[rero+]: https://rero.ch
[fondation-rero+]: https://www.rero.ch/a-propos/la-fondation
[homo]: https://fr.wikipedia.org/wiki/Homo
[minitel2.0]:
https://video.lqdn.fr/videos/watch/edd2b881-f3dd-42e5-b367-42c87c385c07 "Minitel 2.0 par Benjamin Bayart"
[fabriques]: https://www.quaternum.net/2020/04/29/les-fabriques-de-publication/
[rero21]: https://rero21.ch
[reroils]: https://github.com/rero/rero-ils
[sonar]: https://github.com/rero/sonar
[mc]: https://www.unige.ch/biblio/fr/disciplines/medecine-clinique
