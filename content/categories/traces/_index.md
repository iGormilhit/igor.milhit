---
title: "Traces"
date: 2019-09-01T08:00:59+02:00
draft: false
---

La catégorie *traces* fait référence au sous titre de mon ancien blog,
[blogiGor](https://id-libre.org/blogigor), *traces éphémères*. Ce sont des
traces que je voudrais laisser, bien que leur caractère éphémère est indéniable
et m'est précieux, à la fois parce qu'elles témoignent d'instants fugaces et
parce qu'elles-mêmes ne dureront pas.
