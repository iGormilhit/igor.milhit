---
title: "Faire des trucs"
date: 2021-02-14T21:42:49+01:00
draft: false
---

À part faire semblant de faire des choses qu'on fait semblant de croire
qu'elles sont utiles contre un salaire et glander sur le [fediverse][1],
parfois je *fais des trucs*. Et comme il arrive qu'on me demande comment je les
fais, quand j'ai un moment, je les documente ici.

[1]: https://fr.wikipedia.org/wiki/Fediverse
