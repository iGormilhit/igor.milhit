---
title: "Poésie"
date: 2020-05-17:18:54:00
draft: false
---

Une simple manière de rassembler des textes dont la forme est un peu plus
poétique que d'habitude, mais plus on n'y pense moins cette distinction ne
semble avoir de sens, va savoir où se cache la poésie.
