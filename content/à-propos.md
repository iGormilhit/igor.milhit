---
title: "À propos"
date: 2021-02-14T22:06:11+01:00
publishdate: 2021-11-02
draft: false
postimage: "mySite.jpg"
postdescription: "Portrait d'une brebis."
layout: static
slug: à-propos
---

Ce site web à pour but de me présenter à la fois personnellement et
professionnellement, ainsi que de laisser des traces éphémères dans une section
[*blog*][8]. J‘ai expliqué le regroupement de ces deux aspects à l‘ouverture de
ce site, dans le billet [« nouvelle version »][1].

Au départ, il y a l‘envie d‘avoir un coin ou tisser mes bouts de toile sur
cette machine à éditer du texte qu‘est le web. Puis, j‘ai réalisé que je
pouvais en profiter pour avoir la main sur quelles données et éléments que l‘on
trouve en ligne à mon sujet.

Enfin, c‘est aussi un prétexte pour jouer avec des outils qui m‘intriguent,
allez donc savoir pourquoi.

## Crédits

Le site est généré grâce à [Hugo][2], un [générateur de site statique][3]. Il
utilise le thème [*portfoligor*][4] que j‘essaie de réaliser tout seul, et qui
est toujours en chantier.

La palette de couleur que j‘utilise pour ce site sont celles de [nord][7].
C‘est également la palette qui est à la base du thème pour mon environnement de
bureau et pour mon éditeur de texte préféré.

L‘avatar et l‘image du site affichée sur les réseaux sociaux est un recadrage
d‘une belle photo d‘[Annie Spratt][9] :
<https://unsplash.com/photos/6V5iR91KMv0>.

Les sources de ce site web sont également [disponibles en ligne][5]. En dehors
des contenus que je partage et dont je ne suis pas l‘auteur, tout le site peut
être copié, modifié, repris, à condition de me mentionner comme auteur de la
première version, comme le prévoit la licence [{{< smallcaps "cc-by" >}}][6].

Si tu as des questions ou des remarques n‘hésite pas à me contacter [par le
moyen qui te convient le plus][10].

[1]: /nouvelle-version
[2]: https://gohugo.io
[3]: https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_site_statique
[4]: https://framagit.org/iGormilhit/portfoligor "Sources du thème"
[5]: https://framagit.org/iGormilhit/igor.milhit/
[6]: http://creativecommons.org/licenses/by/4.0/
[7]: https://www.nordtheme.com/
[8]: /blog
[9]: https://unsplash.com/@anniespratt
[10]: /#online
